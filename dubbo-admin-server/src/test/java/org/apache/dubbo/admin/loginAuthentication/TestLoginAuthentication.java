package org.apache.dubbo.admin.loginAuthentication;

import org.apache.dubbo.admin.authentication.LoginAuthentication;

import org.apache.dubbo.common.extension.ExtensionLoader;
import org.junit.Test;

import java.util.Iterator;
import java.util.Set;


/**
 * @ClassName TestLoginAuthentication
 * @description:
 * @author: 荣燊
 * @create: 2021-07-22 16:38
 **/
public class TestLoginAuthentication {
    @Test
    public void testConfig() {
        ExtensionLoader<LoginAuthentication> extensionLoader = ExtensionLoader.getExtensionLoader(LoginAuthentication.class);
        Set<LoginAuthentication> supportedExtensionInstances = extensionLoader.getSupportedExtensionInstances();
        System.out.println("supportedExtensionInstances.size:"+supportedExtensionInstances.size());

        Iterator<LoginAuthentication> iterator = supportedExtensionInstances.iterator();
        while (iterator.hasNext()) {
            LoginAuthentication loginAuthentication = iterator.next();
            System.out.println(loginAuthentication.authentication(null, "root1", "root1"));
        }

    }
}
